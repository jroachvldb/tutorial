# README #
This repository is supposed to show the reader how to set up and configure an EC2 instance on AWS (Amazon Web Services) using just Terraform.
All you need to provision an EC2 with in order to connect to is is:

* A security group w/
    * egress (out going traffic)
    * ingress (in coming triffic)
* An internet gateway
* A virtual private cloud
* A machine image (or AMI) w/
    * the region which the AMI is from
* A pair of access and secret keys (provided by AWS account through IAM (identity and Access Management))
* A key pair (a set of security credientials that prove your identity. You need to create this yourself)

This tutorial will provision resources that qualify under the AWS free tier, so don't worry about getting billed for running this code.

## How do I get set up? ##
To follow this tutorial you will need:

An AWS account. (https://aws.amazon.com/free/?trk=ce1f55b8-6da8-4aa2-af36-3f11e9a449ae&sc_channel=ps&sc_campaign=acquisition&sc_medium=ACQ-P%7CPS-GO%7CBrand%7CDesktop%7CSU%7CAWS%7CCore%7CUK%7CEN%7CText&s_kwcid=AL!4422!3!433803620870!e!!g!!create%20aws%20account&ef_id=CjwKCAjwo8-SBhAlEiwAopc9W5kDQ1vqGz_5ErgRMtWn4XgUA9J9MNyH8a-uF8itxqJJ4DFdGYN-DRoC6-IQAvD_BwE:G:s&s_kwcid=AL!4422!3!433803620870!e!!g!!create%20aws%20account&all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc&awsf.Free%20Tier%20Types=*all&awsf.Free%20Tier%20Categories=*all)

The Terraform CLI (0.14.9+) installed. (https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/aws-get-started)

The AWS CLI installed. (https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)

To generate a key pair. (https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/create-key-pairs.html)

To get your access key's secret key. (https://docs.aws.amazon.com/powershell/latest/userguide/pstools-appendix-sign-up.html)

## How to Run the Code ##
If you want to set your access key and secret key to an environment variable, then do the following:

$export AWS_ACCESS_KEY_ID="accesskey"

Then, the secret key:

$export AWS_SECRET_ACCESS_KEY="secretkey"

The values will be sourced from the environment variables so that you don't have to put them inside the main terraform file under the AWS provider block.

You don't have to do this to keep your keys safe. You can also store those keys in a variable file and mark them as 'sensitive', so that they don't show up in log files.
Variable files are just Terraform files that contain a bunch of variables that can be used as a value in the main configuration file. They can be used to hide sensitive values or to just make the code cleaner.

### Initialise the Directory ###

When you make a new configuration you need to initialise the directory with:
'$terraform init'
Initializing a configuration directory downloads and installs the providers defined in the configuration, which in this case is the aws 
provider.

### Format and Validate the Configuration ###
This is section is optional.

The command '$terraform fmt' automatically updates configurations in the current directory by rewritting the code so that is in a canonical format and style.
It helps make code more readble. You can check the difference by using the command '$terraform fmt -check -diff'.

The command '$terraform validate' makes sure that your code is syntactically valid and internally consistent.

### Create Infrastructure ###
Apply the configuration with the $terraform apply command.Before it applies any changes, Terraform prints out the execution plan which describes the actions 
Terraform will take in order to change the infrastructure to match the configuration.

After you confirm the changes, you will have created an infrastructure using TerraForm.

### Inspect State ###
After applying a configuration, TerraForm will write data into a file called 'terraform.tfstate'. This file stores the IDs and properties of the resources it manages
in this file, so that it can update or destroy those resources if need be.

This is the only way TerraForm can track which resources to manage.

## See and Connect to Your Instance ##
If you go to your AWS management console and type "EC2" and click "EC2" under "Services", you should be able to see the active EC2 instance you created with TerraForm.
Check that you are able to connect to it by either SSHing to it via the terminal or by using the built in browser based client, Amazon EC2 Console. If you are timing out or are
unable to connect to it, make sure that you are using a valid AMI that is operable.

# $terraform destroy to delete the instance #