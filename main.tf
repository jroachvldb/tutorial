terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile    = "default"
  region     = var.aws_region
  access_key = var.access_key
  secret_key = var.secret_key
}

resource "aws_instance" "test_server" {
  ami           = "ami-830c94e3"
  instance_type = "t2.micro"
  key_name      = "test_pair"

  tags = {
    Name = "Testing EC2 Instance creation & connection"
  }

  }

resource "aws_vpc" "main" {
  cidr_block = var.aws_vpc_cidr_block
}

resource "aws_internet_gateway" "connect_to_instance" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "Allows you to connect to the instance via the internet."
  }
}

resource "aws_security_group" "test" {
  #vpc_id      = aws_vpc.main.id

  egress = [
    {
      cidr_blocks      = ["0.0.0.0/0"]
      description      = ""
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      self             = false
      protocol         = "-1"
      security_groups  = []
      to_port          = 0
      from_port        = 0
    }
  ]
 ingress                = [
   {
     cidr_blocks      = ["0.0.0.0/0"]
     description      = ""
     ipv6_cidr_blocks = []
     prefix_list_ids  = []
     self             = false
     protocol         = "tcp"
     security_groups  = []
     to_port          = 22
     from_port        = 22
  }
  ]

}

