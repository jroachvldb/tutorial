variable "aws_region" {
  description = "AWS region that you want to use"
  type        = string
  default     = "us-west-2"
}

variable "ami_name" {
  description = "The machine image that you want to use"
  type        = string
  default     = "ami-blahblahblah"
}

variable "access_key" {
  description = "AWS Access Key"
  type        = string
  default     = my_access_key
  sensitive   = true
}

variable "secret_key" {
  description = "AWS Secret Key"
  type        = string
  default     = my_secret_key
  sensitive   = true
}

variable "key_pair" {
  description = "AWS Key Pair"
  type        = string
  default     = "test_pair"
  sensitive   = true
}

variable "aws_vpc_cidr_block"{
  description = "CIDR block for VPC"
  type        = string
  default     = "xxx.xx.x.x/16"
}